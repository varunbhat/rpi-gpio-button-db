#!/usr/bin/env python
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(10, GPIO.IN, pull_up_down=GPIO.PUD_UP)

while True:
    try:
        GPIO.wait_for_edge(10, GPIO.FALLING)
        print "Button Pressed"
        GPIO.wait_for_edge(10, GPIO.RISING)
        print "Button Released"
    except KeyboardInterrupt:
        GPIO.cleanup()
        break




