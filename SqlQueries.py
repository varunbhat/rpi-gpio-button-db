SQL_QUERIES = {
    'PUT_RAW_COUNT': """
        INSERT INTO `raw_count` (`count`) VALUES (%d)""",

    'CONSOLIDATE_RAW_COUNT':
    """SELECT SUM(`count`) FROM `raw_count` where `timestamp` > (CURRENT_TIMESTAMP - INTERVAL 1 HOUR)""",

    'PUT_HOURLY_COUNT':
    """
        INSERT INTO `hourly_count` (`count`) VALUES(%d)
    """,

    'CONSOLIDATE_HOURLY_COUNT':
        """SELECT SUM(`count`) FROM `raw_count` where `timestamp` > (CURRENT_TIMESTAMP - INTERVAL 1 DAY)""",

    'PUT_DAILY_COUNT':
        """
        INSERT INTO `daily_count` (`count`) VALUES(%d)
    """,
    'PURGE_RAW_COUNT':"""DELETE * FROM `raw_count` WHERE `timestamp` > (CURRENT_TIMESTAMP - INTERVAL 1 DAY) """,
    }